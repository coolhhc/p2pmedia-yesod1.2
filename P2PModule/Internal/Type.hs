module P2PModule.Internal.Type (
  peerControllerService,
  Peers,
  PeerState (..)
  ) where

import Import
import qualified Data.Set          as S
import Control.Distributed.Process as DP
import Control.Concurrent.MVar

type Peers = S.Set ProcessId
data PeerState = PeerState { p2pPeers :: MVar Peers }

peerControllerService :: String
peerControllerService = "P2P:Controller"
