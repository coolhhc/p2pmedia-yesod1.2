module Handler.Article where

import Import
import Text.Blaze.Internal (preEscapedText)
import Data.Text (unpack)
import Data.Time (getCurrentTime)
import Control.Concurrent (threadDelay)
import Database.Persist.Sqlite
import Handler.HandlerNode.NodeActions (handlerNode, getP2PNode)
import qualified P2PModule.Internal.Impl as P2P (Action(..))
import Handler.MiniDefaultLayout.Layout (miniDefaultLayout)

---------------------------------------------------------------------------
-- Nav bar version --TODO need to counter duplicate code
---------------------------------------------------------------------------
getSArticleR :: Handler RepHtml
getSArticleR = do
  liftIO $ threadDelay (1 * 1000000)
  queryArticles <- runDB $ selectList ([] :: [Filter QueryArticle]) []
  miniDefaultLayout $ do
      $(widgetFile "article")

-- gets the whole nav bar
getSQueryArticleR :: Handler RepHtml
getSQueryArticleR = do
  queryArticles <- runDB $ selectList ([] :: [Filter QueryArticle]) []
  defaultLayout $ do
      setTitle "P2P Media"
      addStylesheet $ StaticR nav_styles_css
      addScript $ StaticR nav_jquery_js
      $(widgetFile "nav-article")

-- gets the whole nav bar
getSArticleLocalR :: Handler RepHtml
getSArticleLocalR = do
  defaultLayout $ do
      setTitle "P2P Media"
      addStylesheet $ StaticR nav_styles_css
      addScript $ StaticR nav_jquery_js
      $(widgetFile "nav-article-local")

-- dispaly in the display box
getSArticleLocalHamletR :: Handler RepHtml
getSArticleLocalHamletR = do
  articles <- runDB $ selectList ([] :: [Filter Article]) [Desc ArticleTime]
  miniDefaultLayout $ do
      $(widgetFile "article-local")

-- searched display individual articles
getSArticleDisplayR :: QueryArticleId -> Handler RepHtml
getSArticleDisplayR articleId =  do 
  article <- runDB $ get404 articleId
  let titleA   = queryArticleTitle article
      contextA = preEscapedText $ queryArticleContext article
  defaultLayout $ do
      --addStylesheet $ StaticR css_mainstyle_css
      addStylesheet $ StaticR css_normalize_css
      $(widgetFile "article-display")

postSArticleR :: Handler RepHtml
postSArticleR = do
  _ <- runDB $ deleteWhere ([] :: [Filter QueryArticle])
  searchWrapper <- runInputPost $ Search <$> ireq textField "search"
  let query = unpack $ search searchWrapper
  p2pNode <- getP2PNode
  _ <- case p2pNode of
         Just (host, port, hport, _) -> handlerNode (host, hport, host ++ ":" ++ port) (P2P.QueryArticle query)
         Nothing -> notFound
  liftIO $ threadDelay (1 * 1000000)
  redirect SQueryArticleR

-- local display individual articles
getSArticleLocalDisplayR :: ArticleId -> Handler RepHtml
getSArticleLocalDisplayR articleId =  do 
  article <- runDB $ get404 articleId
  let titleA   = articleTitle article
      contextA = preEscapedText $ articleContext article
  defaultLayout $ do
      --addStylesheet $ StaticR css_mainstyle_css
      addStylesheet $ StaticR css_normalize_css
      $(widgetFile "article-display")

getSArticleLocalCreateR :: Handler RepHtml
getSArticleLocalCreateR = do
  miniDefaultLayout $ do
      addScript $ StaticR ckeditor_ckeditor_js
      addStylesheet $ StaticR css_mainstyle_css
      $(widgetFile "article-local-create")

postSArticleLocalCreateR :: Handler RepHtml
postSArticleLocalCreateR = do
  articleForm <- runInputPost $ ArticleForm <$> ireq textField "title" <*> ireq textareaField "editor1"
  now         <- liftIO getCurrentTime
  let titleA  =  title articleForm
      html    =  unTextarea $ context articleForm
  _           <- runDB $ insert $ Article titleA html now
  redirect SArticleLocalR

---------------------------------------------------------------------------
-- Normal version
---------------------------------------------------------------------------
getArticleR :: Handler RepHtml
getArticleR = do
  queryArticles <- runDB $ selectList ([] :: [Filter QueryArticle]) []
  defaultLayout $ do
      setTitle "Search For Article"
      addStylesheet $ StaticR css_mainstyle_css
      $(widgetFile "header")
      $(widgetFile "article")

getArticleDisplayR :: QueryArticleId -> Handler RepHtml
getArticleDisplayR articleId =  do 
  article <- runDB $ get404 articleId
  let titleA   = queryArticleTitle article
      contextA = preEscapedText $ queryArticleContext article
  defaultLayout $ do
      setTitle "Article"
      addStylesheet $ StaticR css_mainstyle_css
      $(widgetFile "header")
      $(widgetFile "article-display")

postArticleR :: Handler RepHtml
postArticleR = do
  _ <- runDB $ deleteWhere ([] :: [Filter QueryArticle])
  searchWrapper <- runInputPost $ Search <$> ireq textField "search"
  let query = unpack $ search searchWrapper
  p2pNode <- getP2PNode
  _ <- case p2pNode of
         Just (host, port, hport, _) -> handlerNode (host, hport, host ++ ":" ++ port) (P2P.QueryArticle query)
         Nothing -> notFound
  liftIO $ threadDelay (1 * 1000000)
  redirect ArticleR

getArticleLocalR :: Handler RepHtml
getArticleLocalR = do
  articles <- runDB $ selectList ([] :: [Filter Article]) [Desc ArticleTime]
  defaultLayout $ do
      setTitle "Local Article"
      addStylesheet $ StaticR css_mainstyle_css
      $(widgetFile "header")
      $(widgetFile "article-local")

getArticleLocalDisplayR :: ArticleId -> Handler RepHtml
getArticleLocalDisplayR articleId =  do 
  article <- runDB $ get404 articleId
  let titleA   = articleTitle article
      contextA = preEscapedText $ articleContext article
  defaultLayout $ do
      setTitle "Article"
      addStylesheet $ StaticR css_mainstyle_css
      $(widgetFile "header")
      $(widgetFile "article-display")

getArticleLocalCreateR :: Handler RepHtml
getArticleLocalCreateR = do
  defaultLayout $ do
      setTitle "Create article"
      addScript $ StaticR ckeditor_ckeditor_js
      addStylesheet $ StaticR css_mainstyle_css
      $(widgetFile "header")
      $(widgetFile "article-local-create")

postArticleLocalCreateR :: Handler RepHtml
postArticleLocalCreateR = do
  articleForm <- runInputPost $ ArticleForm <$> ireq textField "title" <*> ireq textareaField "editor1"
  now         <- liftIO getCurrentTime
  let titleA  =  title articleForm
      html    =  unTextarea $ context articleForm
  _           <- runDB $ insert $ Article titleA html now
  redirect ArticleLocalR

data ArticleForm = ArticleForm {
    title   :: Text,
    context :: Textarea
  }
  deriving Show

data Search = Search { search :: Text }
  deriving Show
