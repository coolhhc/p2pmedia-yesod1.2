{-# LANGUAGE OverloadedStrings, RecordWildCards, DeriveDataTypeable #-}
module Handler.Peers where

import Import
import Handler.PeersParser.ParsePeers (pidsParser)
import Handler.HandlerNode.NodeActions (handlerNode, getP2PNode)
import qualified P2PModule.Internal.Impl as P2P (Action(..))
import Control.Concurrent (threadDelay)

import Handler.MiniDefaultLayout.Layout (miniDefaultLayout)

getPeersR :: Handler RepHtml
getPeersR = do
  liftIO $ threadDelay (3 * 1000000)
  p2pNode <- getP2PNode
  peersString <- case p2pNode of
    Just (host, port, hport, _) -> handlerNode (host, hport, host ++ ":" ++ port) P2P.GetPeers
    Nothing -> notFound
  let Right peers = pidsParser peersString
  defaultLayout $ do
      setTitle "Welcome To Yesod!"
      addStylesheet $ StaticR css_mainstyle_css
      $(widgetFile "header")
      $(widgetFile "peers")

getSPeersR :: Handler RepHtml
getSPeersR = do
  liftIO $ threadDelay (3 * 1000000)
  p2pNode <- getP2PNode
  peersString <- case p2pNode of
    Just (host, port, hport, _) -> handlerNode (host, hport, host ++ ":" ++ port) P2P.GetPeers
    Nothing -> notFound
  let Right peers = pidsParser peersString
  miniDefaultLayout $ do
      $(widgetFile "peers")
