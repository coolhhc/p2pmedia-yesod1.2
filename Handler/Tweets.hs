module Handler.Tweets where

import Import

import Data.Text as T (concat, pack)

getTweetsR :: Handler RepHtml
getTweetsR = do
    let like field val = Filter field (Left $ T.concat ["%", val,"%"]) (BackendSpecificFilter "LIKE")
    ans <- runDB$ selectList [ArticleTitle `like` "Nice"] []

    --ans <- runDB $ selectList ( [] :: [Filter Article] ) []
    defaultLayout $ do
        setTitle "Welcome To Yesod!"
        $(widgetFile "tweets")
