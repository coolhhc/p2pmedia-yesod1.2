module Handler.Load where

import Import
import Control.Concurrent (threadDelay)
import Control.Concurrent (forkIO)
import Handler.MiniDefaultLayout.Layout (miniDefaultLayout)

getLoadR :: Handler RepHtml
getLoadR = do
  miniDefaultLayout $ do
    addStylesheet $ StaticR css_load_css
    $(widgetFile "load")
