module Handler.MiniDefaultLayout.Layout (
  miniDefaultLayout
  ) where

import Import

import Yesod.Default.Config
import Settings (widgetFile, Extra (..))
import Text.Hamlet (hamletFile)

miniDefaultLayout widget = do
  master <- getYesod
  mmsg <- getMessage
  pc <- widgetToPageContent $ do
    $(widgetFile "default-layout")
  hamletToRepHtml $(hamletFile "templates/mini-default-layout-wrapper.hamlet")
