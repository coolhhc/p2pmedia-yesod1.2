{-# LANGUAGE TupleSections, OverloadedStrings #-}
module Handler.Home where

import Import

getHomeR :: Handler RepHtml
getHomeR = do
    defaultLayout $ do
        setTitle "P2P Media"
        addStylesheet $ StaticR nav_styles_css
        addScript $ StaticR nav_jquery_js
        $(widgetFile "nav-homepage")
