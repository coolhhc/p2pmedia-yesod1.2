module Handler.Messages where
import Import
import Database.Persist.Sqlite
import Handler.HandlerNode.NodeActions (handlerNode, getP2PNode)
import qualified P2PModule.Internal.Impl as P2P (Action(..))
import Data.Text (unpack)
import Control.Concurrent (threadDelay)
import Handler.MiniDefaultLayout.Layout (miniDefaultLayout)

-- TODO nav bar logic is not centralized 
-- This function NOT USED
getMessagesR :: Handler RepHtml
getMessagesR = do
    (formWidget, formEnctype) <- generateFormPost msgForm
    msgs <- runDB $ selectList ([] :: [Filter Message]) []
    defaultLayout $ do
        setTitle "Message"
        addStylesheet $ StaticR css_mainstyle_css
        $(widgetFile "header")
        $(widgetFile "messages")

getSMessagesR :: Handler RepHtml
getSMessagesR = do
    liftIO $ threadDelay (3 * 1000000)
    (formWidget, formEnctype) <- generateFormPost msgForm
    msgs <- runDB $ selectList ([] :: [Filter Message]) []
    miniDefaultLayout $ do
        addStylesheet $ StaticR comment_styles_css
        $(widgetFile "messages")

getShowMessagesR :: Handler RepHtml
getShowMessagesR = do
  defaultLayout $ do
      setTitle "P2P Media"
      addStylesheet $ StaticR nav_styles_css
      addScript $ StaticR nav_jquery_js
      $(widgetFile "nav-message")

postMessagesR :: Handler RepHtml
postMessagesR = do
  ((result, _), _) <- runFormPost msgForm
  let formData = case result of
        FormSuccess res ->  Just res 
        _               ->  Nothing
  _ <- case formData of
         Just msgF -> do
           let msg = unpack $ unTextarea $ message msgF
           p2pNode <- getP2PNode
           case p2pNode of
             Just (host, port, hport, _) -> handlerNode (host, hport, host ++ ":" ++ port) (P2P.SendMessage msg)
             Nothing -> notFound
         Nothing -> notFound
  redirect ShowMessagesR

data MsgForm = MsgForm {
  message :: Textarea
  }
  deriving (Show, Eq) 

msgForm :: Form MsgForm
msgForm = renderDivs $ MsgForm
  <$> areq textareaField "Post to feed:" Nothing
