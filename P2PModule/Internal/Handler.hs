{-# LANGUAGE DeriveDataTypeable #-}
module P2PModule.Internal.Handler (
  BinaryArticle(..),
  Action(..),
  HandlerMsg(..)
  ) where

import Import hiding (get, QueryArticle)
import Control.Distributed.Process as DP
import Control.Applicative (Applicative, (<$>), (<*>))
import Control.Monad (liftM)
import Data.Time (UTCTime(..))
import Data.Text (Text)
import Data.Text.Encoding (encodeUtf8, decodeUtf8)
import Data.Typeable (Typeable)
import Data.Binary (Binary(put, get), putWord8, getWord8)

--------------------------------------------------------------
-- BinaryArticle 
--------------------------------------------------------------
data BinaryArticle = BinaryArticle {
  title   :: Text,
  context :: Text,
  time    :: UTCTime
} deriving (Show, Eq, Ord, Typeable)

instance Binary Text where
  put = put . encodeUtf8
  get = liftM decodeUtf8 get 

instance Binary UTCTime where
  put (UTCTime day diff) = do
    put (fromEnum day)
    put (toRational diff)
  get = do
    day  <- get 
    diff <- get 
    return $ UTCTime (toEnum day) (fromRational diff)

instance Binary BinaryArticle where
  put article = put (title article) >> put (context article) >> put (time article)
  get         = BinaryArticle <$> get <*> get <*> get

--------------------------------------------------------------
-- Action, HandlerMsg
--------------------------------------------------------------
data Action = GetPeers
            | SendMessage  { getMsg   :: String }
            | QueryArticle { getQuery :: String }
  deriving (Show, Eq, Ord, Typeable)

data HandlerMsg = HandlerMsg {
    handlerPid    :: ProcessId,
    handlerAction :: Action
  } deriving (Show, Eq, Ord, Typeable)

instance Binary HandlerMsg where
  put hmsg = put (handlerPid hmsg) >> put (handlerAction hmsg)
  get      = HandlerMsg <$> get <*> get 

instance Binary Action where
  put GetPeers            = putWord8 0
  put (SendMessage msg)   = putWord8 1 >> put msg
  put (QueryArticle msg)  = putWord8 2 >> put msg
  get = do
    header <- getWord8
    case header of
      0 -> return GetPeers
      1 -> SendMessage   <$> get
      2 -> QueryArticle  <$> get
      _ -> fail "Identifier.get: invalid"
