{-# LANGUAGE DoAndIfThenElse #-}
module P2PModule.Starter ( 
  p2pStarter,
  p2pRunner
  ) where

import Import
import Yesod.Default.Config
import Control.Concurrent (threadDelay, forkIO)
import Control.Concurrent.MVar (putMVar, takeMVar, MVar)
import P2PModule.Listener (listenerProcess)
import qualified P2PModule.Internal.Impl as P2P
import Bootstrap.DatabaseIO (getBootstrapConfig, clearBootstrapConfig, clearPeersLogs, 
                             clearMessages, clearQueryArticle, runDBIO)

-- database poll time in seconds
pollTime :: Int
pollTime = 5

p2pStarter :: AppConfig DefaultEnv Extra -> MVar (String, String, String) -> IO ()  
p2pStarter conf mvarSets = do
  clearPeersLogs conf
  clearBootstrapConfig conf
  clearMessages conf
  clearQueryArticle conf

  acquireSetting conf

  return ()
  where
    acquireSetting :: AppConfig DefaultEnv Extra -> IO ()
    acquireSetting appConf = do
      isConfig <- haveConfig appConf
      if isConfig then do
        bootstrapSettings <- getBootstrapConfig appConf
        putStrLn $ "bootstrapSettings: " ++ show bootstrapSettings
        putMVar mvarSets bootstrapSettings
        return ()
      else do
        threadDelay (pollTime * 1000000)
        putStrLn $ "bootstrapSettings: Nothing"
        acquireSetting appConf
    haveConfig :: AppConfig DefaultEnv Extra -> IO (Bool)
    haveConfig appConf = do
      appConfig <- runDBIO appConf $ selectFirst ([] :: [Filter Bootstrap]) []
      case appConfig of
        Just _  -> return True
        Nothing -> return False

p2pRunner :: AppConfig DefaultEnv Extra -> MVar (String, String, String) -> IO ()
p2pRunner conf bsSettings = do
  bootstrapSettings <- takeMVar bsSettings
  let (hostBS, portBS, seedsBS) = bootstrapSettings
  _ <- forkIO $ P2P.bootstrap hostBS portBS [P2P.makeNodeId seedsBS] (listenerProcess conf) -- TODO make seed [String]
  return ()
