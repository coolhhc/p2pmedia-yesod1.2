module Handler.PeersParser.ParsePeers ( 
  parsePeers,
  pidsParser
  ) where

import Import
import Database.Persist.Sqlite
import Data.Text as T (unpack)
import GHC.Int
import Text.ParserCombinators.Parsec
import qualified Prelude as P

parsePeers :: [Entity Peer] -> [(GHC.Int.Int64,[String])]
parsePeers [] = []
parsePeers (entity:entities) = let
  num  = entityNum
  Right pids = pidsParser $ T.unpack stringPids
  in (num,pids) : parsePeers entities
  where
    Entity (Key (PersistInt64 entityNum)) (Peer stringPids) = entity

pidsParser :: String -> Either ParseError [String]
pidsParser [] = error "parC: no input"
pidsParser input = parse parsePeersString "(unknown)" $ P.tail input
  where
    parsePeersString :: GenParser Char st [String]
    parsePeersString = do
      result <- cells
      return result
    cells :: GenParser Char st [String]
    cells = do
      first <- cellContent
      next  <- remainingCells
      return (first : next)
    remainingCells :: GenParser Char st [String]
    remainingCells =
      (char ',' >> cells)
      <|> (return []) 
    cellContent :: GenParser Char st String
    cellContent = many (noneOf ",]")
    
